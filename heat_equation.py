#!/usr/bin/env python3
"""
Source: https://surface.syr.edu/cgi/viewcontent.cgi?article=1160&context=eecs_techreports
"""
import os
import numpy
import matplotlib.pyplot as plt


def main():
    # Length of planks [m]
    L_x = 0.25
    L_y = 0.25
    # Thickness of planks [m]
    l = 0.03
    # Number of planks:
    n = 3
    L_z = n * l
    # Heat transfer coefficient perpendicular to fiber direction [W/m²K]
    a = 0.11
    # Heat transfer coefficient parallel to fiber direction [W/m²K]
    b = 0.26

    # Diagonals of Heat transfer coefficient tensor (1 row per layer)
    lambda_ = numpy.empty((n, 3))
    lambda_[0::2, :] = [a, b, a]  # odd layers
    lambda_[1::2, :] = [b, a, a]  # even layers

    # Lattice constant [m]
    G = 2.0e-3

    n_x = int(L_x // G) + 1
    n_y = int(L_y // G) + 1
    n_l = int(l // G) + 1
    n_z = n * n_l
    # Boundary condition
    eps = 10.0  # [K]
    T0 = 273.0 + (
        eps
        * numpy.cos(2.0 * numpy.pi / L_y * numpy.linspace(0.0, L_y, n_y))
        * numpy.ones(n_x)[:, None]
    )
    # Boundary condition
    T1 = numpy.full((n_x, n_y), 293.0)
    # T per domain (plank layer)
    T_dom = numpy.full((n, n_x, n_y, n_l), 0.5 * (numpy.mean(T0) + numpy.mean(T1)))
    load_cache(T_dom)
    show_progress(
        cache(iterable=solve_stationary(T_dom, T0, T1, lambda_), precious=T_dom)
    )

    T = numpy.moveaxis(T_dom, source=0, destination=2).reshape(n_x, n_y, -1)
    x = numpy.linspace(0.0, L_x, n_x)
    y = numpy.linspace(0.0, L_y, n_y)
    z = numpy.linspace(0.0, L_z, n_z)
    grad_T = numpy.array(
        [
            numpy.diff(T, axis=0)[:, :-1, :-1] / (x[1] - x[0]),
            numpy.diff(T, axis=1)[:-1, :, :-1] / (y[1] - y[0]),
            numpy.diff(T, axis=2)[:-1, :-1, :] / (z[1] - z[0]),
        ]
    )
    lambda_z = numpy.repeat(lambda_, n_l, axis=0)
    # If λ is diagonal, it acts as a multiplication operator:
    lambda_grad_T = lambda_z.T[:, None, None, :-1] * grad_T

    # From here on, change to plot coordinates
    pos_x = int(n_x // 2)
    T_yz = T[pos_x, :, :].T
    plot_yz(y, z, T_yz)
    grad_T_yz = grad_T[1:, pos_x, :, :].swapaxes(1, 2)
    plot_grad_yz(y[:-1], z[:-1], grad_T_yz)
    lambda_grad_T_yz = lambda_grad_T[1:, pos_x, :, :].swapaxes(1, 2)
    plot_grad_yz(
        y[:-1],
        z[:-1],
        lambda_grad_T_yz,
        label_prefix="λ",
        scale=50.0,
        unit=r"\frac{W}{m^3}",
    )


def cache(precious, iterable, cache_every: int = 10):
    """Caches an object :precious: every :cache_every:"""
    for iteration, item in enumerate(iterable):
        if iteration % cache_every == 0:
            write_cache(precious)
        yield item
    write_cache(precious)


def show_progress(iterable):
    for iteration, item in enumerate(iterable):
        print(iteration, item)


def solve_stationary(T_dom, T0, T1, lambda_, tolerance: float = 1.0e-3):
    T_new = T_dom.copy()
    l_eff = 0.5 * lambda_ / numpy.sum(lambda_, axis=-1)[:, None]
    n_layers = T_dom.shape[0]
    T_xy0 = numpy.empty((n_layers,) + T0.shape)
    T_xy0[0] = T0
    T_xy1 = numpy.empty((n_layers,) + T0.shape)
    T_xy1[-1] = T1
    # relaxation factor:
    # f_opt = 2 - 2 * numpy.pi / T0.shape[1]
    f_opt = 1
    while True:
        T_xy0[1:] = T_dom[:-1, :, :, -1]
        T_xy1[:-1] = T_dom[1:, :, :, 0]
        residual = relax_diag(
            T_dom,
            l_eff,
            T_xy0,
            T_xy1,
            T_yz0=T_dom[:, -1, :, :],
            T_yz1=T_dom[:, 0, :, :],
            T_xz0=T_dom[:, :, -1, :],
            T_xz1=T_dom[:, :, 0, :],
            f=f_opt,
            out=T_new,
        )
        if residual < tolerance:
            break
        T_dom[()] = T_new
        yield residual


def relax_diag(
    T, l_eff, T_xy0, T_xy1, T_yz0, T_yz1, T_xz0, T_xz1, f: float, out
) -> float:
    # Step 1: Compute weighted average
    average_diag(T, l_eff, T_xy0, T_xy1, T_yz0, T_yz1, T_xz0, T_xz1, out)
    # Step 2: Compute Residual
    out -= T
    residual = numpy.max(numpy.abs(out))
    # Step 3: Compute over relaxation T + f (T_average - T)
    out *= f
    out += T
    return residual


def average_diag(T, l_eff, T_xy0, T_xy1, T_yz0, T_yz1, T_xz0, T_xz1, out):
    """Discrete, weighted Laplacian:
    L u(x,y,z) = [2(λ1+λ2+λ3)u(x,y,z) - λ1 u(x+G,y,z) - λ1 u(x-G,y,z) - λ2 u(x,y+G,z+G) - λ2 u(x,y-G,z) - λ3 u(x,y,z+G) - λ3 u(x,y,z-G)] / G^3
    L u = 0 => u(x,y,z) ~ l1 u(x+G,y,z) + l1 u(x-G,y,z) + l2 u(x,y+G,z+G) + l2 u(x,y-G,z) + l3 u(x,y,z+G) + l3 u(x,y,z-G),
    li = λi/(2(λ1+λ2+λ3)), i=1,2,3
    """
    lx = l_eff[..., 0, None, None]
    ly = l_eff[..., 1, None, None]
    lz = l_eff[..., 2, None, None]
    out[()] = 0.0
    out[..., 1:, :, :] += lx[..., None] * T[..., :-1, :, :]
    out[..., :-1, :, :] += lx[..., None] * T[..., 1:, :, :]
    out[..., :, 1:, :] += ly[..., None] * T[..., :, :-1, :]
    out[..., :, :-1, :] += ly[..., None] * T[..., :, 1:, :]
    out[..., :, :, 1:] += lz[..., None] * T[..., :, :, :-1]
    out[..., :, :, :-1] += lz[..., None] * T[..., :, :, 1:]
    # boundary:
    out[..., 0, :, :] += lx * T_yz0
    out[..., -1, :, :] += lx * T_yz1
    out[..., :, 0, :] += ly * T_xz0
    out[..., :, -1, :] += ly * T_xz1
    out[..., :, :, 0] += lz * T_xy0
    out[..., :, :, -1] += lz * T_xy1


def write_cache(T) -> None:
    try:
        numpy.save(cache_path(T.shape), T)
    except FileNotFoundError:
        os.mkdir(".cache")


def load_cache(out: numpy.ndarray) -> None:
    try:
        out[()] = numpy.load(cache_path(out.shape))
    except FileNotFoundError:
        pass


def cache_path(shape: tuple[int, ...]) -> str:
    return os.path.join(".cache", f"{'-'.join(map(str,shape))}.npy")


def plot_xy(x, y, T):
    """plot T(x,y,z) for z: front, middle and back of each layer"""

    n_layers = T.shape[0]
    n_z = T.shape[3]
    axs = plt.figure(figsize=(10, 8), constrained_layout=True).subplots(3, n_layers)
    for n, (ax_row, T_layer) in enumerate(zip(axs, T)):
        for (ax, pos, label) in zip(
            ax_row, (0, int(n_z // 2), -1), ("front", "middle", "back")
        ):
            countours = ax.contour(*numpy.meshgrid(x, y), T_layer[:, :, pos])
            ax.clabel(countours, inline=True, fontsize=10)
            ax.set_title(f"Layer {n} ({label})")
    plt.show()


def plot_yz(y, z, T_yz):
    """plot T(x,y,z) for x=L_x/2"""

    _fig, ax = plt.subplots()
    contour = ax.contour(y, z, T_yz)
    ax.clabel(contour, inline=True, fontsize=10)
    ax.set_title("T(x=L_x/2, y, z)")
    plt.show()


def plot_grad_yz(
    y,
    z,
    grad_T,
    label_prefix: str = "",
    scale: float = 500.0,
    unit: str = r"\frac{K}{m}",
):
    """plot ∇T(x,y,z), λ∇T(x,y,z) for x=L_x/2"""

    _fig, ax = plt.subplots()
    q = ax.quiver(
        y[::2],
        z[::2],
        grad_T[0, ::2, ::2],
        grad_T[1, ::2, ::2],
        numpy.linalg.norm(grad_T, axis=0)[::2, ::2],
        units="x",
    )
    ax.quiverkey(
        q,
        0.9,
        0.9,
        scale,
        f"${scale} {unit}$",
        labelpos="E",
        coordinates="figure",
    )
    ax.set_title(f"{label_prefix}∇T(x=L_x/2,y,z)")
    plt.show()


if __name__ == "__main__":
    main()
